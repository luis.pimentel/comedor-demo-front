import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppModules } from 'src/app/routing/app-modules';
import { RouterUtils } from 'src/app/shared/model/routing/router-utils';
import { AutenticacionRoutes } from './routing/autenticacion-routes';
import { CuasIniciarSesionComponent } from './components/cuas-iniciar-sesion/cuas-iniciar-sesion.component';

const routes: Routes = [
    {   // localhost:4200/autenticacion
        path: '',
        pathMatch: 'full',
        redirectTo: RouterUtils.getRoute(AppModules.autenticacion, AutenticacionRoutes.inicioSesion)
    },
    {   // localhost:4200/autenticacion/iniciar-sesion
        path: AutenticacionRoutes.inicioSesion.uri.slice(1),
        component: CuasIniciarSesionComponent
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AutenticacionRoutingModule { }
