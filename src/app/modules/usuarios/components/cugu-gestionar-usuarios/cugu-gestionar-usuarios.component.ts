import { AfterViewInit, Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { EmethColumn, EmethColumnsDef } from '@ceneval_plataforma_emeth/ngx-emeth';
import { TranslateService } from '@ngx-translate/core';
import { RouterService } from 'src/app/shared/services/router.service';

@Component({
  selector: 'app-cugu-gestionar-usuarios',
  templateUrl: './cugu-gestionar-usuarios.component.html',
  styleUrls: ['./cugu-gestionar-usuarios.component.scss']
})

export class CuguGestionarUsuariosComponent implements AfterViewInit {

    public usuariosColumnsDef: EmethColumnsDef = new EmethColumnsDef();
    public usuariosDataSource: MatTableDataSource<Usuario> = new MatTableDataSource<Usuario>([]);

    constructor(private translateService:TranslateService,
        private routerService: RouterService) {
        this.setGridDefinition();


    }

    ngAfterViewInit():void{
        console.log("a", this.usuariosColumnsDef.displayedColumns);

        this.loadData();
    }






    public loadData():void{
        this.usuariosDataSource.data = [
            {
                clave:1,
                nombre: "Prueba prueba prueba",
                estado: "Activo"
            },
            {
                clave:2,
                nombre: "Prueba1 prueba1 prueba1",
                estado: "Activo"
            },
            {
                clave:3,
                nombre: "Prueba2 prueb2a prueba2",
                estado: "Desactivado"
            }
        ]
    }

    public setGridDefinition():void{
        this.usuariosColumnsDef.displayedColumns = ['clave', 'nombre', 'estado', 'acciones'];
        this.translateService.get('GU-CU1.tabla').subscribe((label) => {
            this.usuariosColumnsDef.columns.push(new EmethColumn('clave', label.identificador));
            this.usuariosColumnsDef.columns.push(new EmethColumn('nombre', label.nombre));
            this.usuariosColumnsDef.columns.push(new EmethColumn('estado', label.estado));
        })
    }

    public irInicio():void{
        this.routerService.irInicio();
    }



}
interface Usuario{
    clave:number,
    nombre:string,
    estado:string,
}
