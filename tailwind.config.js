module.exports = {
    mode: "jit",
    darkMode: 'class',
    corePlugins: {
        preflight: false,
    },
    content: ["./src/**/*.{html,ts}"],
    safelist: [
        'grid-cols-2',
        'lg:grid-cols-2',
        'grid-cols-3',
        'lg:grid-cols-3',
        'grid-cols-4',
        'lg:grid-cols-4',
        'grid-cols-5',
        'lg:grid-cols-5',
        'grid-cols-6',
        'lg:grid-cols-6',
        'grid-cols-7',
        'lg:grid-cols-7',
        'grid-cols-8',
        'lg:grid-cols-8',
        'grid-cols-9',
        'lg:grid-cols-9',
        'grid-cols-10',
        'lg:grid-cols-10',
        'grid-cols-11',
        'lg:grid-cols1-11',
        'grid-cols-12',
        'lg:grid-cols-12',
        'col-span-1',
        'col-span-2',
        'col-span-3',
        'col-span-4',
        'col-span-5',
        'col-span-6',
        'col-span-7',
        'col-span-8',
        'col-span-9',
        'col-span-10',
        'col-span-11',
        'col-span-12',
        {
            pattern: /m.*/
        }, {
            pattern: /p.*/
        }, {
            pattern: /w.*/
        }, {
            pattern: /flex.*/
        }, {
            pattern: /grid.*/
        }, {
            pattern: /input.*/
        }, {
            pattern: /select.*/
        }, {
            pattern: /font.*/
        }, {
            pattern: /text.*/
        }
    ],
    theme: {
        extend: {
            colors: {
                primary: "#003764",
                secondary: "#FF6C0E",
                correct: "#058835",
                incorrect: "#960101",
                gris: "#00000020"
            },
            transitionProperty: {
                width: "width"
            },
        },
    },
    plugins: [
        require("daisyui")
    ],
    daisyui: {
        themes: [
            {
                'ceneval': {
                    'primary': '#003764',           /* Primary color */
                    'primary-focus': '#002348',     /* Primary color - focused */
                    'primary-content': '#ffffff',   /* Foreground content color to use on primary color */

                    'secondary': '#ff6c0e',         /* Secondary color */
                    'secondary-focus': '#ff4f08',   /* Secondary color - focused */
                    'secondary-content': '#ffffff', /* Foreground content color to use on secondary color */

                    'accent': '#d62121',            /* Accent color */
                    'accent-focus': '#c61414',      /* Accent color - focused */
                    'accent-content': '#ffffff',    /* Foreground content color to use on accent color */

                    'neutral': '#3d4451',           /* Neutral color */
                    'neutral-focus': '#2a2e37',     /* Neutral color - focused */
                    'neutral-content': '#ffffff',   /* Foreground content color to use on neutral color */

                    'base-100': '#ffffff',          /* Base color of page, used for blank backgrounds */
                    'base-200': '#f9fafb',          /* Base color, a little darker */
                    'base-300': '#d1d5db',          /* Base color, even more darker */
                    'base-content': '#1f2937',      /* Foreground content color to use on base color */

                    'info': '#2094f3',              /* Info */
                    'success': '#009485',           /* Success */
                    'warning': '#ff9900',           /* Warning */
                    'error': '#ff2424',             /* Error */
                    'correct': "#058835",           /* Correct */
                    'incorrect': "#960101",         /* Incorrect */
                    'gris': "#00000020"         /* Disabled */
                }
            }
        ]
    }
};
