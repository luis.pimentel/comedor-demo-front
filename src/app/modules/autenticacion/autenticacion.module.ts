import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AutenticacionRoutingModule } from './autenticacion-routing.module';
import { CuasIniciarSesionComponent } from './components/cuas-iniciar-sesion/cuas-iniciar-sesion.component';
import { SharedModule } from 'src/app/shared/modules/shared/shared.module';


@NgModule({
  declarations: [
    CuasIniciarSesionComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    AutenticacionRoutingModule
  ]
})
export class AutenticacionModule { }
