import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsuariosRoutingModule } from './usuarios-routing.module';
import { CuguGestionarUsuariosComponent } from './components/cugu-gestionar-usuarios/cugu-gestionar-usuarios.component';
import { SharedModule } from 'src/app/shared/modules/shared/shared.module';
import { CuguRegistrarUsuarioComponent } from './components/cugu-registrar-usuario/cugu-registrar-usuario.component';


@NgModule({
  declarations: [
    CuguGestionarUsuariosComponent,
    CuguRegistrarUsuarioComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    UsuariosRoutingModule
  ]
})
export class UsuariosModule { }
