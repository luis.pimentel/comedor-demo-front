import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HTTP_INTERCEPTORS, HttpClient, HttpClientModule } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { SharedModule } from './shared/modules/shared/shared.module';
import { EmethModuleConfig, NgxEmethModule } from '@ceneval_plataforma_emeth/ngx-emeth';
import { NavbarComponent } from './components/navbar/navbar.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    SharedModule,
    TranslateModule.forRoot({
        defaultLanguage: 'es',
        loader: {
            provide: TranslateLoader,
            useFactory: (http: HttpClient) => new TranslateHttpLoader(http, './assets/i18n/', '.json'),
            deps: [HttpClient]
        }
    }),
    NgxEmethModule.forRoot({
        actionCards:{
            iconColor:'primary'
        },
        cards:{
            collapsable: false
        },
        contextCardConfig: {
            itemOrientation: 'v',
            columns: 2
        },
        wrapperTable:{
            totalInPageSizeOptions: true
        }
    } as EmethModuleConfig)
  ],
  providers: [
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
