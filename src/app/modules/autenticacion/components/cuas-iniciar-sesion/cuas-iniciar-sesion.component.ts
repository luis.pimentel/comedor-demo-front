import { Component, OnInit } from '@angular/core';
import { AbstractControl, UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { EmethAlertsService } from '@ceneval_plataforma_emeth/ngx-emeth';
import { RouterService } from 'src/app/shared/services/router.service';
import { UserService } from 'src/app/shared/services/user.service';

@Component({
  selector: 'app-cuas-iniciar-sesion',
  templateUrl: './cuas-iniciar-sesion.component.html',
  styleUrls: ['./cuas-iniciar-sesion.component.scss']
})
export class CuasIniciarSesionComponent implements OnInit {

    form: UntypedFormGroup;
    isLoading = false;

    constructor(
        private routerService: RouterService,
        private userService: UserService,
        private alertService: EmethAlertsService,
    ) {
        this.form = new UntypedFormGroup({
            login: new UntypedFormControl('', [Validators.email, Validators.required]),
            password: new UntypedFormControl('', [Validators.required])
        });
    }

    get login(): AbstractControl | null {
        return this.form.get('login');
    }

    get password(): AbstractControl | null {
        return this.form.get('password');
    }

    ngOnInit(): void {
        this.alertService.clearAll();
    }

    // irRecuperarPassword() {
    //     this.routerPerfilador.irRecuperarPassword();
    // }

    submit(): void {
        this.form.markAllAsTouched();
        if (this.form.valid) {
            // this.isLoading = true;
            // const valorFormulario = this.form.getRawValue() as UsuarioInicioSesion;
            // valorFormulario.password = btoa(sha512.sha512(valorFormulario.password));
            // this.controlAcceso.iniciarSesion(valorFormulario).subscribe({
            //     next: (val) => {
            //         this.isLoading = false;
            //         const uData: AppUserData = val;
            //         uData.email = valorFormulario.login;
            //         this.userService.saveUserData(uData);
            //         this.userService.handleCurrentAccount();
            //     },
            //     error: (e) => {
            //         console.error(e);
            //         this.isLoading = false;
            //     }
            // });
            this.routerService.irCuguGestionarUsuarios();

        }
    }

}
