import { RouteContext } from "src/app/shared/model/routing/route-context";

export class UsuariosRoutes{

    static readonly gestionarUsuarios: RouteContext = {
        uri: '/gestionar-usuarios',
        ucName: 'Gestionar usuarios'
    }

}
