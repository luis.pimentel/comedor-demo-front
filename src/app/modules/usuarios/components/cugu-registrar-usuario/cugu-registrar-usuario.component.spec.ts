import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CuguRegistrarUsuarioComponent } from './cugu-registrar-usuario.component';

describe('CuguRegistrarUsuarioComponent', () => {
  let component: CuguRegistrarUsuarioComponent;
  let fixture: ComponentFixture<CuguRegistrarUsuarioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CuguRegistrarUsuarioComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CuguRegistrarUsuarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
