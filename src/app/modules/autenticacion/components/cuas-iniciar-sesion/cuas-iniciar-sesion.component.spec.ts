import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CuasIniciarSesionComponent } from './cuas-iniciar-sesion.component';

describe('CuasIniciarSesionComponent', () => {
  let component: CuasIniciarSesionComponent;
  let fixture: ComponentFixture<CuasIniciarSesionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CuasIniciarSesionComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CuasIniciarSesionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
