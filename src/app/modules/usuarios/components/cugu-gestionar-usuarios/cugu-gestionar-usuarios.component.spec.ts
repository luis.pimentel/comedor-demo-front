import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CuguGestionarUsuariosComponent } from './cugu-gestionar-usuarios.component';

describe('CuguGestionarUsuariosComponent', () => {
  let component: CuguGestionarUsuariosComponent;
  let fixture: ComponentFixture<CuguGestionarUsuariosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CuguGestionarUsuariosComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CuguGestionarUsuariosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
