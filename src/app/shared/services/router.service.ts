import { Injectable } from "@angular/core";
import { Params, Router } from "@angular/router";
import { RouterUtils } from "../model/routing/router-utils";
import { AppModules } from "src/app/routing/app-modules";
import { UsuariosRoutes } from "src/app/modules/usuarios/routing/usuarios-routes";
import { AutenticacionRoutes } from "src/app/modules/autenticacion/routing/autenticacion-routes";

@Injectable({
  providedIn: "root",
})
export class RouterService {
    private pila: Array<string> = [];
    constructor(private router: Router) {}

    //Ir inicio
    irInicio(){
        return this.ir(
            RouterUtils.getRoute(
                AppModules.autenticacion,
                AutenticacionRoutes.inicioSesion
            )
        )
    }

    // Ir gestionar usuarios
    irCuguGestionarUsuarios(){
        return this.ir(
            RouterUtils.getRoute(
                AppModules.usuarios,
                UsuariosRoutes.gestionarUsuarios
            )
        )
    }



    ir(url: string, queryParamsObject?: unknown): Promise<boolean> {
        this.pila.push(this.router.url);
        return this.router.navigate([url], {
            queryParams: queryParamsObject as Params
        });
    }
}
