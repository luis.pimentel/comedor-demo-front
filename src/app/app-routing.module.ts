import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppModules } from './routing/app-modules';
import { RouterUtils } from './shared/model/routing/router-utils';
import { AutenticacionRoutes } from './modules/autenticacion/routing/autenticacion-routes';

const routes: Routes = [

    {
        path: AppModules.autenticacion.id,
        loadChildren: () => import('./modules/autenticacion/autenticacion.module')
            .then(m => m.AutenticacionModule)
    },
    {
        path: AppModules.usuarios.id,
        loadChildren: () => import('./modules/usuarios/usuarios.module')
            .then(m => m.UsuariosModule)
    },
    {
        path: '',
        pathMatch: 'full',
        redirectTo: RouterUtils.getRoute(AppModules.autenticacion, AutenticacionRoutes.inicioSesion)
    },
    {
        path: '**',
        pathMatch: 'full',
        redirectTo: RouterUtils.getRoute(AppModules.autenticacion, AutenticacionRoutes.inicioSesion)
    }

];

@NgModule({
  imports: [RouterModule.forRoot(routes, {scrollPositionRestoration: "enabled"})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
