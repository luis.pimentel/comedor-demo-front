import { ModuleContext } from "../shared/model/routing/module-context";

export class AppModules {

    static readonly autenticacion: ModuleContext = {
        id: 'autenticacion',
        description: 'Modulo de autenticación'
    }

    static readonly usuarios: ModuleContext = {
        id: 'usuarios',
        description: 'Modulo de usuarios'
    }

}
