import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UsuariosRoutes } from './routing/usuarios-routes';
import { CuguGestionarUsuariosComponent } from './components/cugu-gestionar-usuarios/cugu-gestionar-usuarios.component';

const routes: Routes = [

    {
        path: UsuariosRoutes.gestionarUsuarios.uri.slice(1),
        component: CuguGestionarUsuariosComponent
    }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsuariosRoutingModule { }
