import { RouteContext } from "src/app/shared/model/routing/route-context";

export class AutenticacionRoutes{
    static readonly inicioSesion: RouteContext = {
        uri: '/iniciar-sesion',
        ucName: 'Inicio de sesión'
    }
}
